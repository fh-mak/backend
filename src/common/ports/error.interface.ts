export interface IAppError<TData = unknown> {
	readonly message: TData
}
