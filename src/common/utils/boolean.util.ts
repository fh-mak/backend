import { isBoolean as _isBoolean } from 'lodash-es'

/**
 * Check if the `value` is classified as a boolean primitive or object.
 */
function isBoolean(value: unknown): value is boolean {
	return _isBoolean(value)
}

export { isBoolean }
