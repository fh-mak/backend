import { Expose } from 'class-transformer'
import { IsNumber, IsString, MinLength } from 'class-validator'

import { getEVsFactory } from './_helper.env.js'

class MongoEVs {
	@IsString()
	@MinLength(1)
	@Expose({ name: 'MONGO_USER' })
	public user: string

	@IsString()
	@MinLength(8)
	@Expose({ name: 'MONGO_PASSWORD' })
	public password: string

	@IsString()
	@Expose({ name: 'MONGO_HOST' })
	public host: string

	@IsNumber()
	@Expose({ name: 'MONGO_PORT' })
	public port: number

	@IsString()
	@Expose({ name: 'MONGO_DATABASE' })
	public database: string
}

const mongoEVsFactory = getEVsFactory('mongo', () => new MongoEVs())
export { mongoEVsFactory, MongoEVs }
