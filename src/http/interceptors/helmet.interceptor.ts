import { Injectable } from '@nestjs/common'
import { default as helmet } from 'helmet'

import type {
	NestInterceptor,
	ExecutionContext,
	CallHandler,
} from '@nestjs/common'
import type {
	IncomingMessage as Request,
	ServerResponse as Response,
} from 'http'

@Injectable()
export class HelmetInterceptor implements NestInterceptor {
	public constructor(
		private readonly _helmetOptions?: Parameters<typeof helmet>[0],
	) {}

	public intercept(context: ExecutionContext, next: CallHandler) {
		if (context.getType() !== 'http') return next.handle()

		const ctx = context.switchToHttp()
		const request = ctx.getRequest<Request>()
		const response = ctx.getResponse<Response>()

		this._ensureSetHeaderMethodExists(response)

		helmet(this._helmetOptions)(request, response, () => {
			// It is not necessary to pass the `next` function
			// because it is already handled by the `NestInterceptor`
		})

		return next.handle()
	}

	// NOTE: The `helmet` function uses the `setHeader` method to modify any header,
	// but the `FastifyRely` class does not have this method
	// (it uses the `header` method instead),
	// so a `setHeader` method needs to be added which internally uses the `header` method.
	private _ensureSetHeaderMethodExists(response: Response) {
		const setHeaderProp = Reflect.get(response, 'setHeader') as unknown
		if (setHeaderProp instanceof Function) return

		const searchingKeys = ['header']
		searchingKeys.every(key => {
			const prop = Reflect.get(response, key) as unknown
			if (!(prop instanceof Function)) return true

			Reflect.set(response, 'setHeader', prop)
			return false
		})
	}
}
