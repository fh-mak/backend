import { BaseEntity, EntityId } from '@@domain/core/index.js'
import { CreateUserError } from '@@errors/index.js'
import { fail, Result, success } from '@@result/index.js'

import { Email, IEmailProps } from './email.value-object.js'
import { Password, IPasswordProps } from './password.value-object.js'
import { Username, IUsernameProps } from './username.value-object.js'

type IUserProps = {
	username: Readonly<IUsernameProps>
	password: Readonly<IPasswordProps>
	email: Readonly<IEmailProps>
}

class User extends BaseEntity<IUserProps> {
	private constructor(props: IUserProps, id?: EntityId) {
		super(props, id)
	}

	public get email() {
		return this.__props.email
	}
	public get username() {
		return this.__props.username
	}
	public get password() {
		return this.__props.password
	}

	public static create(userProps: IUserProps): Result<CreateUserError, User> {
		const usernameResult = Username.validate(userProps.username)
		const passwordResult = Password.validate(userProps.password)
		const emailResult = Email.validate(userProps.email)

		const errorData = {
			username: usernameResult.isFailure()
				? usernameResult.error.message
				: undefined,
			password: passwordResult.isFailure()
				? passwordResult.error.message
				: undefined,
			email: emailResult.isFailure()
				? emailResult.error.message
				: undefined,
		} as CreateUserError['message']

		if (errorData.username || errorData.email || errorData.password) {
			return fail(new CreateUserError(errorData))
		}
		return success(new User(userProps))
	}
}

export { User }
export type { IUserProps }
