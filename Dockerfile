ARG TAG

FROM node:$TAG as base
RUN npm install --global pnpm

FROM base as build
WORKDIR /fh

COPY . .
RUN pnpm install && \
	pnpm mi:create && \
	pnpm mi:up && \
	pnpm build:prod && \
	pnpm prune --prod --config.ignore-scripts=true && \
	pnpm store prune

FROM base as run
WORKDIR /fh

COPY --from=build /fh/dist ./dist
COPY --from=build /fh/node_modules ./node_modules
COPY --from=build /fh/package.json /fh/*.env ./

ENV NODE_ENV=production
EXPOSE 3000

CMD pnpm start:prod
