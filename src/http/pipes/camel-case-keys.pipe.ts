import { Injectable } from '@nestjs/common'

import { isPlainObject, toCamelCaseKeys } from '@@utils/object.util.js'

import type { PipeTransform } from '@nestjs/common'

@Injectable()
export class CamelCaseKeysPipe implements PipeTransform {
	public transform(value: unknown) {
		return isPlainObject(value) ? toCamelCaseKeys(value) : value
	}
}
