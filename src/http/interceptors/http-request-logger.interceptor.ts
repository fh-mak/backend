import { Injectable } from '@nestjs/common'

import type {
	NestInterceptor,
	ExecutionContext,
	CallHandler,
} from '@nestjs/common'
import type {
	IncomingMessage as Request,
	ServerResponse as Response,
} from 'http'

@Injectable()
export class HttpRequestLoggerInterceptor implements NestInterceptor {
	public async intercept(context: ExecutionContext, next: CallHandler) {
		if (context.getType() !== 'http') return next.handle()

		const ctx = context.switchToHttp()
		const request = ctx.getRequest<Request>()
		const response = ctx.getResponse<Response>()

		const { default: morgan } = await import('morgan')
		morgan('combined')(request, response, () => {
			// It is not necessary to pass the `next` function
			// because it is already handled by the `NestInterceptor`
		})

		return next.handle()
	}
}
