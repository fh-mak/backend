import { BaseValueObject } from '@@domain/core/index.js'
import { EmailInvalidFormatError } from '@@errors/index.js'
import { Result, success, fail } from '@@result/index.js'
import { isEmail } from '@@utils/string.util.js'

type IEmailProps = string

class Email extends BaseValueObject {
	private constructor(private readonly _email: IEmailProps) {
		super()
	}

	public get email() {
		return this._email
	}

	public static validate(
		props: IEmailProps,
	): Result<EmailInvalidFormatError, void> {
		if (isEmail(props)) {
			return success(undefined)
		}
		return fail(new EmailInvalidFormatError())
	}

	public static create(
		props: IEmailProps,
	): Result<EmailInvalidFormatError, Email> {
		const result = this.validate(props)
		if (result.isFailure()) return result
		return success(new Email(props))
	}
}

export { Email }
export type { IEmailProps }
