import { randomBytes } from 'crypto'

type EntityId = string | symbol
const uuid = () => randomBytes(16).toString('hex')

abstract class BaseEntity<TProps> {
	protected __props: TProps
	private readonly _id: EntityId

	protected constructor(props: TProps, id?: EntityId) {
		this.__props = props
		this._id = id ?? uuid()
	}

	public equals(other: BaseEntity<TProps>) {
		return this._id === other._id
	}
}

export { BaseEntity }
export type { EntityId }
