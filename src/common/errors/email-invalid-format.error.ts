import { HttpStatus, HttpStatusMeta } from '@@decorators/index.js'

import type { IAppError } from '@@ports/index.js'

@HttpStatusMeta(HttpStatus.BAD_REQUEST)
export class EmailInvalidFormatError implements IAppError<string> {
	public readonly message: string
	public constructor() {
		this.message = `email is invalid format`
	}
}
