import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

import { EnvServiceProvider } from '@@http/providers/index.js'

import type { IEnvService } from '@@http/providers/index.js'
import type { INestApplication } from '@nestjs/common'

function configSwagger(app: INestApplication) {
	const envService = app.get<IEnvService>(EnvServiceProvider.provide)
	const { nodeEnv, tag } = envService.get('server', { infer: true })
	if (nodeEnv === 'production') return

	const options = new DocumentBuilder()
		.setTitle('API')
		.setVersion(tag)
		.addBearerAuth()
		.build()
	const document = SwaggerModule.createDocument(app, options)
	SwaggerModule.setup('/rest-api', app, document)
}

export { configSwagger }
