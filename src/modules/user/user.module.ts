import { Module } from '@nestjs/common'

import { CreateUserConverterProvider } from '@@presenters/index.js'

import {
	CreateUserController,
	CreateUserUseCaseProvider,
} from './create-user-use-case/index.js'

@Module({
	controllers: [CreateUserController],
	providers: [CreateUserUseCaseProvider, CreateUserConverterProvider],
})
export class UserModule {}
