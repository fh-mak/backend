# 👻 Description

Backend of the `FH` e-commerce website, using [`Docker`](https://www.docker.com), [`NestJS`](https://nestjs.com), [`MongoDB`](https://www.mongodb.com), [`Postgres`](https://www.postgresql.org) and [`Redis`](https://redis.io).

# 🐊 Requirements

- [`NodeJS`](https://nodejs.org): `18.14.x` ([`fnm`](https://github.com/Schniz/fnm) or [`a node version manager support .node-version`](https://github.com/shadowspawn/node-version-usage#supporting-products))
- [`Docker Engine`](https://docs.docker.com/engine): `20.10.x`
- [`Docker Compose`](https://docs.docker.com/compose): `2.16.x`
- [`pnpm`](https://pnpm.io): `8.1.x` ([`npm`](https://docs.npmjs.com) or [`yarn`](https://classic.yarnpkg.com))

# 🐉 Setup

## 💫 Install NodeJS:

Install manually: https://nodejs.org/en/download or

Install through a node version manager:

```sh
# fnm
$ fnm install
```

## 📦 Install packages:

```sh
# pnpm
$ pnpm install

# yarn
$ yarn install

# npm
$ npm install

```

## 🐳 Run Docker:
```sh
# See the table below to understand the meanings of each environment variable
$ cp .example.env .env
$ docker-compose up
```

### 🧩 Environment variables table

|            Name            |            Using service            | Metadata                                    | Security level  | Description                                                         |
|:--------------------------:|:-----------------------------------:|:--------------------------------------------|:---------------:|:--------------------------------------------------------------------|
|    COMPOSE_PROJECT_NAME    | [all](/docker-compose.yml#L2)       |   optional: ✅ <br> default: $PWD           |       🟢       | the project name                                                    |
|      COMPOSE_PROFILES      | [all](/docker-compose.yml#L2)       |   optional: ✅ <br> default: ''             |       🟢       | the list of service name to be enabled when running docker-compose  |
|           APP_TAG          | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'latest'       |       🟢       | the specified tag to use for the fh/backend image                   |
|          APP_PORT          | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 3000           |       🟢       | the port that the server service listens on                         |
|          NODE_ENV          | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'development'  |       🟢       | 'production', 'testing', 'staging' or 'development' environment     |
|          NODE_TAG          | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'lts-alpine'   |       🟢       | the specified tag to use for the node image                         |
|          MONGO_TAG         | [mongo](/docker-compose.yml#L18)    |   optional: ✅ <br> default: 'jammy'        |       🟢       | the specified tag to use for the mongo image                        |
|         MONGO_USER         | [mongo](/docker-compose.yml#L18)    |   optional: ❌ <br> min-length: 1           |       🟠       | the superuser username for the mongo image                          |
|       MONGO_PASSWORD       | [mongo](/docker-compose.yml#L18)    |   optional: ❌ <br> min-length: 8           |       🔴       | the superuser password for the mongo image                          |
|         MONGO_HOST         | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'localhost'    |       🟢       | the Mongo host name for the fh/backend image                        |
|         MONGO_PORT         | [mongo](/docker-compose.yml#L18)    |   optional: ✅ <br> default: 27017          |       🟢       | the port that the mongo service listens on                          |
|       MONGO_DATABASE       | [mongo](/docker-compose.yml#L18)    |   optional: ✅ <br> default: 'fh'           |       🟠       | the init database name for the mongo image                          |
|        POSTGRES_TAG        | [postgres](/docker-compose.yml#L33) |   optional: ✅ <br> default: 'alpine'       |       🟢       | the specified tag to use for the postgres image                     |
|        POSTGRES_USER       | [postgres](/docker-compose.yml#L33) |   optional: ❌ <br> min-length: 1           |       🟠       | the superuser username for the postgres image                       |
|      POSTGRES_PASSWORD     | [postgres](/docker-compose.yml#L33) |   optional: ❌ <br> min-length: 8           |       🔴       | the superuser password for the postgres image                       |
|        POSTGRES_HOST       | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'localhost'    |       🟢       | the Postgres host name for the fh/backend image                     |
|        POSTGRES_PORT       | [postgres](/docker-compose.yml#L33) |   optional: ✅ <br> default: 5432           |       🟢       | the port that postgres service listens on                           |
|      POSTGRES_DATABASE     | [postgres](/docker-compose.yml#L33) |   optional: ✅ <br> default: 'fh'           |       🟠       | the init database name for the postgres image                       |
|          REDIS_TAG         | [redis](/docker-compose.yml#L47)    |   optional: ✅ <br> default: 'alpine'       |       🟢       | the specified tag to use for the redis image                        |
|         REDIS_USER         | [redis](/docker-compose.yml#L47)    |   optional: ❌ <br> min-length: 1           |       🟠       | the masteruser username for the redis image (needs to be fix)       |
|       REDIS_PASSWORD       | [redis](/docker-compose.yml#L47)    |   optional: ❌ <br> min-length: 8           |       🔴       | the masteruser password for the redis image                         |
|         REDIS_HOST         | [server](/docker-compose.yml#L3)    |   optional: ✅ <br> default: 'localhost'    |       🟢       | the Redis host name for the fh/backend image                        |
|         REDIS_PORT         | [redis](/docker-compose.yml#L47)    |   optional: ✅ <br> default: '6379'         |       🟢       | the port that the redis service listens on                          |

# 🎮 Scripts

## 🐈 Running

```sh
# development
$ pnpm start:dev

# watch mode
$ pnpm start:watch

# production mode
$ pnpm build:prod && pnpm start:prod
```

## 🍪 Testing

```bash
# unit tests
$ pnpm test

# e2e tests
$ pnpm test:e2e

# test coverage
$ pnpm test:cov
```

## 🦈 Formatting & Linting
```bash
# format
$ pnpm format

# lint
$ pnpm lint
```

# ※ License

[GNU licensed](LICENSE).
