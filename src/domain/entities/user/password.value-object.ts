import { BaseValueObject } from '@@domain/core/index.js'
import { PasswordTooShortError } from '@@errors/index.js'
import { Result, fail, success } from '@@result/index.js'

type IPasswordProps = string

class Password extends BaseValueObject {
	private static readonly _MIN_LENGTH = 8

	private constructor(private readonly _password: IPasswordProps) {
		super()
	}

	public get password() {
		return this._password
	}

	public static validate(
		props: IPasswordProps,
	): Result<PasswordTooShortError, void> {
		if (props.length < this._MIN_LENGTH) {
			return fail(new PasswordTooShortError(this._MIN_LENGTH))
		}
		return success(undefined)
	}

	public static create(
		props: IPasswordProps,
	): Result<PasswordTooShortError, Password> {
		const result = this.validate(props)
		if (result.isFailure()) return result
		return success(new Password(props))
	}
}

export { Password }
export type { IPasswordProps }
