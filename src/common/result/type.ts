import type {
	BaseResult,
	FailedResult,
	Result,
	SuccessfulResult,
} from './result.js'
import type { ExcludeValueFromArray } from '@@types/index.js'

type ToObject<TResult extends Result> = TResult extends BaseResult<
	infer TError,
	infer TValue
>
	? {
			error: TError
			value: TValue
	  }
	: never

/** Get an error of a Result */
type ErrorOf<TResult extends Result> = ToObject<TResult>['error']
/** Get an error array of a Result array include success results */
type ErrorsOf<TArray extends Result[]> = {
	[Index in keyof TArray]: ErrorOf<TArray[Index]>
}
/** Get an error array of a Result array exclude success results */
type ErrorsExcludeOf<TArray extends Result[]> = ErrorsOf<
	ExcludeValueFromArray<TArray, SuccessfulResult>
>

/** Get a value of a Result */
type ValueOf<TResult extends Result> = ToObject<TResult>['value']
/** Get a value array of a Result array include failed results  */
type ValuesOf<TArray extends readonly Result[]> = {
	[Index in keyof TArray]: ValueOf<TArray[Index]>
}
/** Get a value array of a Result array exclude failed results */
type ValuesExcludeOf<TArray extends readonly Result[]> = ValuesOf<
	ExcludeValueFromArray<TArray, FailedResult>
>

export type {
	ErrorOf,
	ErrorsOf,
	ErrorsExcludeOf,
	ValueOf,
	ValuesOf,
	ValuesExcludeOf,
}
