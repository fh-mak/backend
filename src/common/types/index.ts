import type { ClassProvider as NestClassProvider } from '@nestjs/common'
import type { Promisable, Class } from 'type-fest'

type AbstractClassName<
	T extends object = object,
	TArguments extends unknown[] = unknown[],
> = abstract new (...args: TArguments) => T

type SingleOrArray<T> = T | T[]

type ExcludeValueFromDynamicArray<
	TArray extends readonly unknown[],
	TValue,
> = TArray extends (infer TElement)[] ? Exclude<TElement, TValue>[] : []

type ExcludeValueFromFixedArray<
	TArray extends readonly unknown[],
	TValue,
> = TArray extends [infer TFirst, ...infer TRest]
	? TFirst extends TValue
		? ExcludeValueFromFixedArray<TRest, TValue>
		: [TFirst, ...ExcludeValueFromFixedArray<TRest, TValue>]
	: []

type ExcludeValueFromArray<
	TArray extends readonly unknown[],
	TValue,
> = IsDynamicArray<TArray> extends true
	? ExcludeValueFromDynamicArray<TArray, TValue>
	: ExcludeValueFromFixedArray<TArray, TValue>

type PartialExclude<T, TKey extends keyof T> = Partial<T> & Pick<T, TKey>
type NonEmptyPartial<T> = {
	[TKkey in keyof T]-?: PartialExclude<T, TKkey>
}[keyof T]

type IsDynamicArray<TArray extends readonly unknown[]> =
	number extends TArray['length'] ? true : false

type ClassToPlain<T> = {
	[K in keyof T]: T[K] extends object ? ClassToPlain<T[K]> : T[K]
}

type ClassProvider<T = unknown> = NestClassProvider<T>

export type {
	AbstractClassName,
	Class as ClassName,
	SingleOrArray,
	ExcludeValueFromArray,
	IsDynamicArray,
	Promisable,
	ClassToPlain,
	ClassProvider,
	NonEmptyPartial,
}
