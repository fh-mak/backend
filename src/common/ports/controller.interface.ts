import type { IAnyUseCase } from '@@ports/index.js'

export interface IController<TUseCase extends IAnyUseCase> {
	readonly useCase: TUseCase
	execute(...args: unknown[]): unknown
}
