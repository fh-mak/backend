import { ConfigModule as NestEnvModule } from '@nestjs/config'
import { plainToInstance } from 'class-transformer'
import { validateSync } from 'class-validator'

import { evsFactories, GlobalEVs, globalEVsFactory } from '@@http/env/index.js'
import { EnvServiceProvider } from '@@http/providers/index.js'

import type { ClassName } from '@@types/index.js'

const envModuleOptions = {
	envFilePath: ['.env', '.default.env'],
	cache: true,
	expandVariables: true,
	isGlobal: true,
	load: [globalEVsFactory],
	validate: (evs: Record<string, unknown>) => {
		const validatingEVs = evsFactories.reduce((globalEvs, factory) => {
			const className = factory().constructor as ClassName<
				(typeof globalEvs)[typeof factory.token]
			>
			const option = {
				excludeExtraneousValues: true,
				enableImplicitConversion: true,
			}
			Object.defineProperty(globalEvs, factory.token, {
				value: plainToInstance(className, evs, option),
			})

			return globalEvs
		}, new GlobalEVs())

		const errors = validateSync(validatingEVs, {
			skipMissingProperties: false,
		})

		if (errors.length > 0) {
			throw new Error(errors.toString())
		}

		return validatingEVs
	},
}

type EnvModuleOptions = Omit<
	NonNullable<Parameters<(typeof NestEnvModule)['forRoot']>[0]>,
	keyof typeof envModuleOptions
>

class EnvModule extends NestEnvModule {
	public static override forRoot(options?: EnvModuleOptions) {
		const envModule = NestEnvModule.forRoot({
			...options,
			...envModuleOptions,
		})
		envModule.providers?.push(EnvServiceProvider)
		envModule.exports?.push(EnvServiceProvider)

		return envModule
	}

	public static override forFeature(config: (typeof evsFactories)[number]) {
		const envModule = NestEnvModule.forFeature(config)
		return envModule
	}
}

export { EnvModule }
