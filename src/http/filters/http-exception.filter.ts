import { HttpException, Catch } from '@nestjs/common'

import { toPlainObject } from '@@utils/object.util.js'

import type { ExceptionFilter, ArgumentsHost } from '@nestjs/common'
import type { HttpAdapterHost } from '@nestjs/core'

/**
 * Adding the `timestamp` and `path` properties to all response exceptions.
 */
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
	public constructor(private readonly _httpAdapterHost: HttpAdapterHost) {}

	public catch(exception: HttpException, host: ArgumentsHost) {
		const { httpAdapter } = this._httpAdapterHost

		const ctx = host.switchToHttp()
		const statusCode = exception.getStatus()
		const response = exception.getResponse()

		const responseBody = {
			...toPlainObject(response, 'message'),
			timestamp: new Date().toISOString(),
			path: httpAdapter.getRequestUrl(ctx.getRequest()) as string,
		}

		httpAdapter.reply(ctx.getResponse(), responseBody, statusCode)
	}
}
