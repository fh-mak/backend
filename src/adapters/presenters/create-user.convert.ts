import { HttpException } from '@nestjs/common'

import { getHttpStatus } from '@@decorators/index.js'
import { ExceptionDto } from '@@dtos/index.js'
import { reduce } from '@@utils/object.util.js'

import type { CreateUserError } from '@@errors/index.js'
import type { IOneWayConverter } from '@@ports/index.js'
import type { ClassProvider } from '@@types/index.js'

type ICreateUserConverter = IOneWayConverter<CreateUserError, HttpException>
const CreateUserConverterToken = Symbol('ICreateUserConverter')

class CreateUserConverter implements ICreateUserConverter {
	public toInfra = (error: CreateUserError) => {
		const status = getHttpStatus(error)
		const exceptionDto = this._toExceptionDto(error)
		return new HttpException(exceptionDto, status)
	}

	private _toExceptionDto(error: CreateUserError) {
		const message = reduce(
			error.message,
			(message, value, key) => ({
				...message,
				[key]: value ? [value] : undefined,
			}),
			{} as Exclude<ExceptionDto['message'], string>,
		)
		return new ExceptionDto(message)
	}
}

const CreateUserConverterProvider = {
	provide: CreateUserConverterToken,
	useClass: CreateUserConverter,
} as ClassProvider<CreateUserConverter>

export { CreateUserConverterToken, CreateUserConverterProvider }
export type { ICreateUserConverter }
