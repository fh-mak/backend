import type { IAppError } from '@@ports/index.js'
import type { Result } from '@@result/index.js'

interface IUseCase<
	TParameter extends Record<PropertyKey, unknown> | undefined,
	TError extends IAppError | undefined,
	TValue,
	TIsAsync extends boolean = true,
> {
	execute(
		parameter: TParameter,
	): TIsAsync extends true
		? Promise<Result<TError, TValue>>
		: Result<TError, TValue>
}

type IAnyUseCase = IUseCase<
	Record<PropertyKey, unknown> | undefined,
	IAppError | undefined,
	unknown,
	boolean
>

type ToObject<TUseCase extends IAnyUseCase> = TUseCase extends IUseCase<
	infer TParameter,
	infer TError,
	infer TValue,
	infer TIsAsync
>
	? {
			parameter: TParameter
			error: TError
			value: TValue
			isAsync: TIsAsync
	  }
	: never

type ParameterOf<TUseCase extends IAnyUseCase> = ToObject<TUseCase>['parameter']
type ErrorOf<TUseCase extends IAnyUseCase> = ToObject<TUseCase>['error']
type ValueOf<TUseCase extends IAnyUseCase> = ToObject<TUseCase>['value']
type IsAsyncOf<TUseCase extends IAnyUseCase> = ToObject<TUseCase>['isAsync']

type ResultOf<TUseCase extends IAnyUseCase> = Result<
	ErrorOf<TUseCase>,
	ValueOf<TUseCase>
>

type ReturnOf<TUseCase extends IAnyUseCase> = IsAsyncOf<TUseCase> extends true
	? Promise<ResultOf<TUseCase>>
	: ResultOf<TUseCase>

export type {
	IUseCase,
	IAnyUseCase,
	ParameterOf,
	ErrorOf,
	ValueOf,
	IsAsyncOf,
	ResultOf,
	ReturnOf,
}
