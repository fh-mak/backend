import { HttpStatus, HttpStatusMeta } from '@@decorators/index.js'

import type { IAppError } from '@@ports/index.js'

@HttpStatusMeta(HttpStatus.BAD_REQUEST)
export class UsernameTooShortError implements IAppError<string> {
	public readonly message: string
	public constructor(minLength = 8) {
		this.message = `username must be longer than ${minLength} characters`
	}
}
