import { castArray as _castArray, isArray } from 'lodash-es'

import type { SingleOrArray } from '@@types/index.js'

/**
 * Cast the `value` to an array if it is not an array.
 */
function toArray<T>(value?: SingleOrArray<T>) {
	return _castArray<T>(value)
}

export { toArray, isArray }
