import type { INestApplication } from '@nestjs/common'
import type { NestExpressApplication } from '@nestjs/platform-express'

export function configProxy(app: INestApplication) {
	const expressApp = app as NestExpressApplication
	expressApp.enable('trust proxy')
}
