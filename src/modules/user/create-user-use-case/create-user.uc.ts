import { IUserProps, User } from '@@entities/user/index.js'
import { success } from '@@result/index.js'

import type { CreateUserError } from '@@errors/index.js'
import type { IUseCase } from '@@ports/index.js'
import type { ClassProvider } from '@@types/index.js'

const AddUserUseCaseToken = Symbol('IAddUseUseCase')
type IAddUserUseCase = IUseCase<IUserProps, CreateUserError, void, false>

class AddUserUseCase implements IAddUserUseCase {
	public execute(userProps: IUserProps) {
		const result = User.create(userProps)

		if (result.isFailure()) {
			return result
		}

		return success()
	}
}

const AddUserUseCaseProvider = {
	provide: AddUserUseCaseToken,
	useClass: AddUserUseCase,
} as ClassProvider<AddUserUseCase>

export {
	AddUserUseCaseProvider as CreateUserUseCaseProvider,
	AddUserUseCaseToken,
}
export type { IAddUserUseCase }
