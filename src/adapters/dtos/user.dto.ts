import { IsString } from 'class-validator'

export class UserDto {
	@IsString()
	public username: string

	@IsString()
	public password: string

	@IsString()
	public email: string
}
