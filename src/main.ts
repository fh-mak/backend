import { NestFactory } from '@nestjs/core'

import {
	configProxy,
	configCors,
	configSwagger,
	useGlobalFilters,
	useGlobalInterceptors,
	useGlobalPipes,
} from '@@http/config/index.js'
import { AppModule } from '@@http/modules/index.js'
import { EnvServiceToken } from '@@http/providers/index.js'

import type { IEnvService } from '@@http/providers/index.js'

async function bootstrap() {
	const app = await NestFactory.create(AppModule)

	configProxy(app)
	configCors(app)
	configSwagger(app)
	useGlobalInterceptors(app)
	useGlobalPipes(app)
	useGlobalFilters(app)
	app.enableShutdownHooks()

	const envService = app.get<IEnvService>(EnvServiceToken)
	const port = envService.get('server.port', { infer: true })
	await app.listen(port, () =>
		console.log(`The app is listening on port ${port}.`),
	)
}

void bootstrap()
