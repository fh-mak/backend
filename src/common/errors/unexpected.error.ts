import { HttpStatus, HttpStatusMeta } from '@@decorators/index.js'

import type { IAppError } from '@@ports/index.js'

@HttpStatusMeta(HttpStatus.INTERNAL_SERVER_ERROR)
export class UnexpectedError implements IAppError<string> {
	public readonly message: string
	public constructor(message = 'An unexpected error occurred.') {
		this.message = message
	}
}
