import { isNumber as _isNumber } from 'lodash-es'

/**
 * Checks if value is classified as a Number primitive or object.
 */
function isNumber(value: unknown): value is boolean {
	return _isNumber(value)
}

/**
 * If the `value` is a number, return it.
 * Otherwise use the `converter` to convert it to a number.
 */
function castNumber(value: unknown, converter: (value: unknown) => number) {
	return isNumber(value) ? value : converter(value)
}

export { isNumber, castNumber }
