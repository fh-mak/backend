import { BadRequestException, ValidationPipe } from '@nestjs/common'

import { CamelCaseKeysPipe } from '@@http/pipes/index.js'
import { values } from '@@utils/object.util.js'

import type { ExceptionDto } from '@@dtos/index.js'
import type { INestApplication } from '@nestjs/common'
import type { ValidationError } from 'class-validator'

function flattenValidationError(
	errors: ValidationError[],
	prevPrefix = '',
): Exclude<ExceptionDto['message'], string> {
	return errors.reduce((messages, error) => {
		const currentPrefix = `${prevPrefix}${error.property}`
		const nextPrefix = `${currentPrefix}.`
		return {
			...messages,
			[currentPrefix]: values(error.constraints),
			...flattenValidationError(error.children ?? [], nextPrefix),
		}
	}, {})
}

export function useGlobalPipes(app: INestApplication) {
	const pipes = [
		new CamelCaseKeysPipe(),

		// NOTE: `HttpExceptionFilter` can catch it
		// because by default it throws a `BadRequestException`.
		// LINK: src\infra\filters\http-exception.filter.ts
		new ValidationPipe({
			// REF: https://docs.nestjs.com/techniques/validation#stripping-properties
			whitelist: true,
			// REF: https://docs.nestjs.com/custom-decorators#working-with-pipes
			validateCustomDecorators: true,
			exceptionFactory: (errors: ValidationError[]) =>
				new BadRequestException({
					message: flattenValidationError(errors),
				} as ExceptionDto),
		}),
	]
	app.useGlobalPipes(...pipes)
}
