class ExceptionDto {
	public constructor(
		public message: Record<string, string[] | undefined> | string,
	) {}
}

class FullExceptionDto extends ExceptionDto {
	public timestamp: Date
	public path: string
}

export { ExceptionDto, FullExceptionDto }
