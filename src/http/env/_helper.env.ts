import { registerAs } from '@nestjs/config'

import type { ConfigObject, ConfigFactory } from '@nestjs/config'

type EVsFactory<
	TToken extends string,
	TConfig extends ConfigObject,
	TFactory extends ConfigFactory,
> = ReturnType<typeof registerAs<TConfig, TFactory>> & {
	token: TToken
}

function getEVsFactory<
	TToken extends string,
	TConfig extends ConfigObject,
	TFactory extends ConfigFactory = ConfigFactory<TConfig>,
>(token: TToken, configFactory: TFactory) {
	type Factory = EVsFactory<TToken, TConfig, TFactory>
	const evsFactory = registerAs(token, configFactory) as Factory
	evsFactory.token = token
	return evsFactory
}

export { getEVsFactory }
