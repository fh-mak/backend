import { FailedResult, SuccessfulResult } from './result.js'

import type { Result } from './result.js'
import type { ErrorsExcludeOf, ValuesExcludeOf } from './type.js'
import type { IsDynamicArray } from '@@types/index.js'

function success<TValue>(value: TValue): SuccessfulResult<TValue>
function success(): SuccessfulResult<void>
function success<TValue>(value?: TValue) {
	return new SuccessfulResult(value)
}

function fail<TError>(error: TError): FailedResult<TError>
function fail(): FailedResult<void>
function fail<TError>(error?: TError) {
	return new FailedResult(error)
}

/**
 * @example
 * combineResults<[SuccessResult<string>, SuccessResult<number>, FailedResult<boolean>]>
 * => FailedResult<[boolean]>
 *
 * combineResults<[SuccessResult<string>, SuccessResult<number>, SuccessResult<number>]>
 * => SuccessResult<[string, number, number]>
 *
 * combineResults<SuccessResult<string>[]>
 * => SuccessResult<string[]>
 *
 * combineResults<(SuccessfulResult<symbol> | SuccessfulResult<number> | FailedResult<boolean>)[]>
 * => FailedResult<boolean[]> | SuccessfulResult<(number | symbol)[]>
 */
function combineResults<TResults extends Result[]>(
	...results: TResults
): Results<TResults> {
	const failedResults = results.filter(result =>
		result.isFailure(),
	) as FailedResult[]
	const errors = failedResults.map(result => result.error)
	if (errors.length > 0) {
		return fail(errors) as Results<TResults>
	}

	const successfulResults = results as SuccessfulResult[]
	const values = successfulResults.map(result => result.value)
	return success(values) as Results<TResults>
}

type Results<TResults extends Result[]> = IsDynamicArray<TResults> extends true
	? Result<ErrorsExcludeOf<TResults>, ValuesExcludeOf<TResults>>
	: ErrorsExcludeOf<TResults>['length'] extends 0
	? SuccessfulResult<ValuesExcludeOf<TResults>>
	: FailedResult<ErrorsExcludeOf<TResults>>

export { success, fail, combineResults }
