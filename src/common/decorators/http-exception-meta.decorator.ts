import type { IAppError } from '@@ports/index.js'
import type { ClassName } from '@@types/index.js'

const HTTP_EXCEPTION = Symbol('HTTP_EXCEPTION')

function HttpStatusMeta(status: number) {
	return function (useCaseClassName: ClassName<object>) {
		Reflect.defineMetadata(HTTP_EXCEPTION, status, useCaseClassName)
	}
}

function getHttpStatus<TUseCaseError extends IAppError = IAppError>(
	useCase: ClassName<TUseCaseError> | TUseCaseError,
) {
	return Reflect.getMetadata(
		HTTP_EXCEPTION,
		useCase instanceof Function ? useCase : useCase.constructor,
	) as number
}

export { HttpStatusMeta, getHttpStatus }
export { HttpStatus } from '@nestjs/common'
