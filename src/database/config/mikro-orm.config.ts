import { readFileSync } from 'fs'

import { ReflectMetadataProvider } from '@mikro-orm/core'
import { defineConfig } from '@mikro-orm/postgresql'
import { parse } from 'dotenv'

const env = (function getEnv(envPath: string, defaultEnvPath: string) {
	const rw = readFileSync(envPath, { encoding: 'utf-8' })
	const env = parse(rw)

	const defaultRaw = readFileSync(defaultEnvPath, { encoding: 'utf-8' })
	const defaultEnv = parse(defaultRaw)

	return {
		...defaultEnv,
		...env,
	}
})('.env', '.default.env')

const mikroOrmConfig = defineConfig({
	user: env.POSTGRES_USER,
	password: env.POSTGRES_PASSWORD,
	host: env.POSTGRES_HOST,
	port: parseInt(env.POSTGRES_PORT, 10),
	dbName: env.POSTGRES_DATABASE,
	ensureDatabase: true,
	metadataProvider: ReflectMetadataProvider,
	entities: ['./dist/database/models'],
	entitiesTs: ['./src/database/models'],
	migrations: {
		path: './dist/database/migrations',
		pathTs: './src/database/migrations',
	},
})

export default mikroOrmConfig
