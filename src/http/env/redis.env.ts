import { Expose } from 'class-transformer'
import { IsNumber, IsString, MinLength } from 'class-validator'

import { getEVsFactory } from './_helper.env.js'

@Expose()
class RedisEVs {
	@IsString()
	@MinLength(1)
	@Expose({ name: 'REDIS_USER' })
	public username: string

	@IsString()
	@MinLength(8)
	@Expose({ name: 'REDIS_PASSWORD' })
	public password: string

	@IsNumber()
	@Expose({ name: 'REDIS_PORT' })
	public port: number
}

const redisEVsFactory = getEVsFactory('redis', () => new RedisEVs())
export { redisEVsFactory, RedisEVs }
