import { HttpStatusMeta, HttpStatus } from '@@decorators/index.js'

import type {
	UsernameTooShortError,
	PasswordTooShortError,
	EmailInvalidFormatError,
} from '@@errors/index.js'
import type { IAppError } from '@@ports/index.js'

interface ICreateUserErrorData {
	username?: UsernameTooShortError['message']
	password?: PasswordTooShortError['message']
	email?: EmailInvalidFormatError['message']
}

@HttpStatusMeta(HttpStatus.BAD_REQUEST)
export class CreateUserError implements IAppError<ICreateUserErrorData> {
	public readonly message: ICreateUserErrorData
	public constructor(message: ICreateUserErrorData) {
		this.message = message
	}
}
