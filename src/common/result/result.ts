abstract class BaseResult<TError, TValue> {
	protected constructor(
		private readonly _isSuccess: false | true,
		protected readonly __data: TError | TValue,
	) {}

	public isSuccess(): this is SuccessfulResult<TValue> {
		return this._isSuccess
	}

	public isFailure(): this is FailedResult<TError> {
		return !this._isSuccess
	}
}

class SuccessfulResult<TValue = unknown> extends BaseResult<never, TValue> {
	public constructor(value: TValue) {
		super(true, value)
	}

	public get value() {
		return this.__data
	}
}

class FailedResult<TError = unknown> extends BaseResult<TError, never> {
	public constructor(error: TError) {
		super(false, error)
	}

	public get error() {
		return this.__data
	}
}

type Result<TError = unknown, TValue = unknown> = TError extends
	| never[]
	| null
	| undefined
	? SuccessfulResult<TValue>
	: FailedResult<TError> | SuccessfulResult<TValue>

export { BaseResult, FailedResult, SuccessfulResult }
export type { Result }
