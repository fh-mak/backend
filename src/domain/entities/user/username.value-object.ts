import { BaseValueObject } from '@@domain/core/index.js'
import { UsernameTooShortError } from '@@errors/index.js'
import { Result, fail, success } from '@@result/index.js'

type IUsernameProps = string

class Username extends BaseValueObject {
	private static readonly _MIN_LENGTH = 8

	private constructor(private readonly _username: IUsernameProps) {
		super()
	}

	public get username() {
		return this._username
	}

	public static validate(
		props: IUsernameProps,
	): Result<UsernameTooShortError, void> {
		if (props.length < this._MIN_LENGTH) {
			return fail(new UsernameTooShortError(this._MIN_LENGTH))
		}
		return success(undefined)
	}

	public static create(
		props: IUsernameProps,
	): Result<UsernameTooShortError, Username> {
		const result = this.validate(props)
		if (result.isFailure()) return result
		return success(new Username(props))
	}
}

export { Username }
export type { IUsernameProps }
