import camelCaseKeys from 'camelcase-keys'
import _deepEqual from 'fast-deep-equal'
import {
	isNil as _isNil,
	values as _values,
	keys as _keys,
	isPlainObject as _isPlainObject,
	isEmpty,
	reduce,
} from 'lodash-es'

/** Checks if `value` is a plain object (not a function, array or class). */
function isPlainObject<TValue>(
	value: unknown,
): value is Record<PropertyKey, TValue> {
	return _isPlainObject(value)
}

/**
 * If the `value` is an pure object (see {@link isPlainObject}), return it.
 * Otherwise, cast it to an object with the provided `key`.
 */
function toPlainObject(value: unknown, key: string) {
	return isPlainObject(value)
		? value
		: {
				[key]: value,
		  }
}

/** Checks if `value` is `null` or `undefined`. */
function isNil(value: unknown): value is null | undefined {
	return _isNil(value)
}

/** Convert all keys of the `object` to camelCase. */
function toCamelCaseKeys<T extends object>(object: T) {
	return camelCaseKeys(object)
}

/** Deep equal */
function deepEqual(lhs: object, rhs: object) {
	return _deepEqual(lhs, rhs)
}

/** Get values of a object */
function values<T>(object: T) {
	return _values(object) as unknown[]
}

/** Get keys of a object */
function keys<T>(object: T) {
	return _keys(object) as (keyof T)[]
}

export {
	isEmpty,
	isPlainObject,
	toPlainObject,
	isNil,
	toCamelCaseKeys,
	values,
	keys,
	deepEqual,
	reduce,
}
