import { HttpAdapterHost } from '@nestjs/core'

import { HttpExceptionFilter } from '@@http/filters/index.js'

import type { INestApplication } from '@nestjs/common'

export function useGlobalFilters(app: INestApplication) {
	const adapterHost = app.get(HttpAdapterHost)
	app.useGlobalFilters(new HttpExceptionFilter(adapterHost))
}
