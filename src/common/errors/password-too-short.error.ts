import { HttpStatus, HttpStatusMeta } from '@@decorators/index.js'

import type { IAppError } from '@@ports/index.js'

@HttpStatusMeta(HttpStatus.BAD_REQUEST)
export class PasswordTooShortError implements IAppError<string> {
	public readonly message: string
	public constructor(minLength = 8) {
		this.message = `password must have at least ${minLength} characters.`
	}
}
