import { ConfigService as NestEnvService } from '@nestjs/config'

import type { globalEVsFactory } from '@@http/env/index.js'
import type { ClassProvider } from '@@types/index.js'
import type { ConfigType } from '@nestjs/config'

const EnvServiceToken = Symbol('IEnvService')

type IEnvService = NestEnvService<ConfigType<typeof globalEVsFactory>, true>

class EnvService
	extends NestEnvService<ConfigType<typeof globalEVsFactory>, true>
	implements IEnvService {}

const EnvServiceProvider = {
	provide: EnvServiceToken,
	useClass: EnvService,
} as ClassProvider<EnvService>

export { EnvServiceProvider, EnvServiceToken }
export type { IEnvService }
