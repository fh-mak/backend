import { isEmail } from 'class-validator'
import { isString as _isString } from 'lodash-es'

/**
 * Checks if the `value` is classified as a String primitive or object.
 */
function isString(value: unknown): value is string {
	return _isString(value)
}

/**
 * Return the lowercase form of the `value` if it is a string.
 * Otherwise return the value itself.
 */
function toLowercaseNoThrow<T>(value: T) {
	return isString(value) ? value.toLowerCase() : value
}

export { isString, toLowercaseNoThrow, isEmail }
