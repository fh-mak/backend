interface IOneWayConverter<TUseCase extends object, TInfra extends object> {
	toInfra: (data: TUseCase) => TInfra
}

interface ITwoWayConverter<TUseCase extends object, TInfra extends object>
	extends IOneWayConverter<TUseCase, TInfra> {
	toUseCase: (data: TInfra) => TUseCase
}

export type { IOneWayConverter, ITwoWayConverter }
