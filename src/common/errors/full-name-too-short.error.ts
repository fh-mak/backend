import { HttpStatus, HttpStatusMeta } from '@@decorators/index.js'

import type { IAppError } from '@@ports/index.js'

@HttpStatusMeta(HttpStatus.BAD_REQUEST)
export class FullNameTooShortError implements IAppError<string> {
	public readonly message: string
	public constructor(minLength = 8) {
		this.message = `The full name must have at least ${minLength} characters`
	}
}
