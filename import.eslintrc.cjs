module.exports = {
	settings: {
		'import/resolver': {
			typescript: true,
			node: true,
		},
	},
	plugins: ['import'],
	extends: [
		'plugin:import/recommended',
		'plugin:import/typescript',
	],
	rules: {
		//#region overrides import/recommended

		// 'import/no-unresolved'
		// 'import/named'
		'import/namespace': 'off', //  typescript compiler
		// 'import/default'
		'import/export': 'off', // typescript compiler

		'import/no-named-as-default': 'error',
		'import/no-named-as-default-member': 'error',
		'import/no-duplicates': 'error',

		//#endregion
		//#region helpful warnings

		'import/no-deprecated': 'error',
		'import/no-empty-named-blocks': 'error',
		'import/no-extraneous-dependencies': 'off', // not care
		'import/no-mutable-exports': 'error',
		'import/no-unused-modules': 'off', // not care

		//#endregion
		//#region module systems

		'import/no-amd': 'off', // @typescript-eslint/no-require-imports
		'import/no-commonjs': 'off', // @typescript-eslint/no-require-imports
		'import/no-import-module-exports': 'error',
		'import/no-nodejs-modules': 'off', // NOTE: enable for frontend
		'import/unambiguous': 'off', // not care

		//#endregion
		//#region static analysis

		'import/no-absolute-path': 'error',
		'import/no-cycle': 'off', // not care
		'import/no-dynamic-require': 'off', // not care
		'import/no-internal-modules': [
			'error',
			{
				forbid: ['@@*/**/!(index.js|index|*.util.js)', '@@*/!(index.js|index|*.util.js)']
			},
		],
		'import/no-relative-packages': 'off', // import/no-internal-modules
		'import/no-relative-parent-imports': 'off', // not working with alias path
		'import/no-restricted-paths': 'off', // TODO: change this rule depend on the structure of each project
		'import/no-self-import': 'error',
		'import/no-useless-path-segments': 'error',
		'import/no-webpack-loader-syntax': 'off', // not care

		//#endregion
		//#region style guide

		'import/consistent-type-specifier-style': 'error',
		'import/dynamic-import-chunkname': 'off', // not care
		'import/exports-last': 'error',
		'import/extensions': 'off', // typescript compiler
		'import/first': 'error',
		'import/group-exports': 'error',
		'import/max-dependencies': 'off', // not care
		'import/newline-after-import': 'off', // prettier/prettier
		'import/no-anonymous-default-export': 'error',
		'import/no-default-export': 'off', // not care
		'import/no-named-default': 'off', // not care
		'import/no-named-export': 'off', // not care
		'import/no-namespace': 'off', // not care
		'import/no-unassigned-import': [
			'error',
			{
				allow: ['reflect-metadata'],
			}
		], // NOTE: allow .css, .js,... files for  frontend
		'import/order': [
			'error',
			{
				groups: [
					'builtin',
					'external',
					'internal',
					'parent',
					'sibling',
					'index',
					'object',
					'type',
					'unknown',
				],
				'newlines-between': 'always',
				alphabetize: {
					order: 'asc',
					caseInsensitive: true,
				},
			},
		],
		'import/prefer-default-export': 'off', // not care

		//#endregion
	},
}
