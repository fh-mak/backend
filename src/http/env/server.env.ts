import { Expose } from 'class-transformer'
import { IsNumber, IsString, IsIn } from 'class-validator'

import { getEVsFactory } from './_helper.env.js'

const nodeEnv = ['production', 'testing', 'staging', 'development'] as const
type NodeEnv = (typeof nodeEnv)[number]

@Expose()
class ServerEVs {
	@IsString()
	@IsIn(nodeEnv)
	@Expose({ name: 'NODE_ENV' })
	public nodeEnv: NodeEnv

	@IsString()
	@Expose({ name: 'APP_TAG' })
	public tag: string

	@IsNumber()
	@Expose({ name: 'APP_PORT' })
	public port: number
}

const serverEVsFactory = getEVsFactory('server', () => new ServerEVs())

export { serverEVsFactory, ServerEVs }
