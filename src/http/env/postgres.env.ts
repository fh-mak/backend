import { Expose } from 'class-transformer'
import { IsNumber, IsString, MinLength } from 'class-validator'

import { getEVsFactory } from './_helper.env.js'

@Expose()
class PostgresEVs {
	@IsString()
	@MinLength(1)
	@Expose({ name: 'POSTGRES_USER' })
	public user: string

	@IsString()
	@MinLength(8)
	@Expose({ name: 'POSTGRES_PASSWORD' })
	public password: string

	@IsString()
	@Expose({ name: 'POSTGRES_HOST' })
	public host: string

	@IsNumber()
	@Expose({ name: 'POSTGRES_PORT' })
	public port: number

	@IsString()
	@Expose({ name: 'POSTGRES_DATABASE' })
	public database: string
}

const postgresEVsFactory = getEVsFactory('postgres', () => new PostgresEVs())
export { postgresEVsFactory, PostgresEVs }
