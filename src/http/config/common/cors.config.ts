import type { INestApplication } from '@nestjs/common'

export function configCors(app: INestApplication) {
	app.enableCors({
		// TODO: change to the frontend domain
		origin: true,
		credentials: true,
	})
}
