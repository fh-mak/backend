import { deepEqual } from '@@utils/object.util.js'

export abstract class BaseValueObject {
	public equals(other: BaseValueObject) {
		return deepEqual(this, other)
	}
}
