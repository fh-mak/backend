import { Expose, Type } from 'class-transformer'
import { ValidateNested } from 'class-validator'

import { getEVsFactory } from './_helper.env.js'
import { MongoEVs, mongoEVsFactory } from './mongo.env.js'
import { PostgresEVs, postgresEVsFactory } from './postgres.env.js'
import { RedisEVs, redisEVsFactory } from './redis.env.js'
import { ServerEVs, serverEVsFactory } from './server.env.js'

@Expose()
class GlobalEVs {
	@ValidateNested()
	@Type(() => ServerEVs)
	public server: ServerEVs = new ServerEVs()

	@ValidateNested()
	@Type(() => MongoEVs)
	public mongo: MongoEVs = new MongoEVs()

	@ValidateNested()
	@Type(() => PostgresEVs)
	public postgres: PostgresEVs = new PostgresEVs()

	@ValidateNested()
	@Type(() => RedisEVs)
	public redis: RedisEVs = new RedisEVs()
}

const globalEVsFactory = getEVsFactory('', () => new GlobalEVs())

const evsFactories = [
	serverEVsFactory,
	mongoEVsFactory,
	postgresEVsFactory,
	redisEVsFactory,
] as const

export {
	// global
	globalEVsFactory,
	GlobalEVs,

	// partial
	serverEVsFactory,
	mongoEVsFactory,
	postgresEVsFactory,
	redisEVsFactory,

	// array
	evsFactories,
}
