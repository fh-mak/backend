import {
	HelmetInterceptor,
	HttpRequestLoggerInterceptor,
} from '@@http/interceptors/index.js'
import { EnvServiceToken } from '@@http/providers/index.js'

import type { IEnvService } from '@@http/providers/index.js'
import type { INestApplication } from '@nestjs/common'

export function useGlobalInterceptors(app: INestApplication) {
	const envService = app.get<IEnvService>(EnvServiceToken)
	const nodeEnv = envService.get('server.nodeEnv', { infer: true })

	const interceptors = [
		new HelmetInterceptor(),
		nodeEnv !== 'production' ? new HttpRequestLoggerInterceptor() : null,
	].filter(interceptor => interceptor !== null) as Parameters<
		(typeof app)['useGlobalInterceptors']
	>
	app.useGlobalInterceptors(...interceptors)
}
