import { MikroOrmModule } from '@mikro-orm/nestjs'
import { Module } from '@nestjs/common'

import { EnvModule } from '@@http/modules/index.js'
import { useCaseModules } from '@@modules/index.js'

@Module({
	imports: [EnvModule.forRoot(), MikroOrmModule.forRoot(), ...useCaseModules],
})
export class AppModule {}
