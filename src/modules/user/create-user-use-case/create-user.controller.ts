import {
	Body,
	Controller,
	HttpCode,
	Inject,
	Post,
	HttpStatus,
} from '@nestjs/common'
import { ApiResponse } from '@nestjs/swagger'

import { getHttpStatus } from '@@decorators/index.js'
import { UserDto } from '@@dtos/index.js'
import { CreateUserError } from '@@errors/index.js'
import { CreateUserConverterToken } from '@@presenters/index.js'

import { AddUserUseCaseToken } from './create-user.uc.js'

import type { IAddUserUseCase } from './create-user.uc.js'
import type { IController } from '@@ports/index.js'
import type { ICreateUserConverter } from '@@presenters/index.js'

@Controller('/user')
export class CreateUserController implements IController<IAddUserUseCase> {
	public constructor(
		@Inject(AddUserUseCaseToken)
		public readonly useCase: IAddUserUseCase,

		@Inject(CreateUserConverterToken)
		public readonly converter: ICreateUserConverter,
	) {}

	@Post()
	@HttpCode(HttpStatus.CREATED)
	@ApiResponse({
		status: getHttpStatus(CreateUserError),
		description:
			'`username`, `password` and/or `email` is/are not valid format.',
	})
	public execute(@Body() userDto: UserDto) {
		const result = this.useCase.execute(userDto)
		if (result.isSuccess()) {
			return
		}

		const exception = this.converter.toInfra(result.error)
		throw exception
	}
}
