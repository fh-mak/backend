module.exports = {
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: true,
	},
	plugins: ['@typescript-eslint'],
	extends: [
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
	],
	rules: {
		//#region overrides @typescript-eslint/recommended

		// @typescript-eslint/adjacent-overload-signatures
		// @typescript-eslint/ban-ts-comment
		// @typescript-eslint/ban-types
		'@typescript-eslint/no-array-constructor': 'off',
		'@typescript-eslint/no-empty-function': [
			'error',
			{
				allow: [
					'decoratedFunctions',
					'protected-constructors',
					'private-constructors',
				],
			},
		],
		'@typescript-eslint/no-empty-interface': 'off',
		'@typescript-eslint/no-explicit-any': 'error',
		// @typescript-eslint/no-extra-non-null-assertion
		// @typescript-eslint/no-extra-semi // conflict with Prettier
		// @typescript-eslint/no-inferrable-types
		// @typescript-eslint/no-loss-of-precision
		// @typescript-eslint/no-misused-new
		// @typescript-eslint/no-namespace
		// @typescript-eslint/no-non-null-asserted-optional-chain
		'@typescript-eslint/no-non-null-assertion': 'error',
		// @typescript-eslint/no-this-alias
		// @typescript-eslint/no-unnecessary-type-constraint
		'@typescript-eslint/no-unused-vars': [
			'error',
			{ argsIgnorePattern: 'ignored', args: 'after-used' },
		],
		// @typescript-eslint/no-var-requires
		// @typescript-eslint/prefer-as-const
		// @typescript-eslint/prefer-namespace-keyword
		// @typescript-eslint/triple-slash-reference

		//#endregion
		//#region overrides @typescript-eslint/recommended-requiring-type-checking

		//@typescript-eslint/await-thenable
		//@typescript-eslint/no-floating-promises
		//@typescript-eslint/no-for-in-array
		//@typescript-eslint/no-implied-eval
		//@typescript-eslint/no-misused-promises
		//@typescript-eslint/no-unnecessary-type-assertion
		//@typescript-eslint/no-unsafe-argument
		//@typescript-eslint/no-unsafe-assignment
		//@typescript-eslint/no-unsafe-call
		//@typescript-eslint/no-unsafe-member-access
		//@typescript-eslint/no-unsafe-return
		//@typescript-eslint/require-await
		//@typescript-eslint/restrict-plus-operands
		'@typescript-eslint/restrict-template-expressions': 'off',
		//@typescript-eslint/unbound-method

		//#endregion
		//#region supported rules

		'@typescript-eslint/array-type': 'error',
		'@typescript-eslint/ban-tslint-comment': 'error',
		'@typescript-eslint/class-literal-property-style': 'error',
		'@typescript-eslint/consistent-generic-constructors': 'error',
		'@typescript-eslint/consistent-indexed-object-style': 'off', // not care
		'@typescript-eslint/consistent-type-assertions': 'off', // not care
		'@typescript-eslint/consistent-type-definitions': 'off', // not care
		'@typescript-eslint/consistent-type-exports': 'error',
		'@typescript-eslint/consistent-type-imports': 'off', // import/consistent-type-specifier-style work better
		'@typescript-eslint/explicit-function-return-type': 'off', // not care
		'@typescript-eslint/explicit-member-accessibility': 'error',
		'@typescript-eslint/explicit-module-boundary-types': 'off', // not care
		// @typescript-eslint/member-delimiter-style // conflict with Prettier
		'@typescript-eslint/member-ordering': 'error',
		'@typescript-eslint/method-signature-style': 'off', // not care
		'@typescript-eslint/naming-convention': [
			'error',
			{
				selector: ['default'],
				format: ['PascalCase'],
				types: ['boolean'],
				prefix: ['is', 'should', 'has', 'can', 'did', 'will'],
			},
			{
				selector: ['variableLike', 'memberLike'],
				format: ['camelCase'],
			},
			{
				selector: 'variable',
				modifiers: ['const'],
				format: ['camelCase', 'UPPER_CASE'],
			},
			{
				selector: 'variable',
				format: ['PascalCase'],
				filter: {
					"regex": "(ClassName|Provider|Token)$",
					"match": true
				}
			},
			{
				selector: 'function',
				format: ['camelCase', 'PascalCase'], //decorator
			},
			{
				selector: 'memberLike',
				format: ['camelCase'],
				modifiers: ['private'],
				prefix: ['_'],
			},
			{
				selector: 'memberLike',
				format: ['camelCase'],
				modifiers: ['protected'],
				prefix: ['__'],
			},
			{
				selector: 'typeLike',
				format: ['PascalCase'],
			},
			{
				selector: 'property',
				format: ['UPPER_CASE'],
				modifiers: ['private', 'readonly', 'static'],
				prefix: ['_'],
			},
			{
				selector: 'property',
				format: ['UPPER_CASE'],
				modifiers: ['protected', 'readonly', 'static'],
				prefix: ['__'],
			},
			{
				selector: 'property',
				format: ['UPPER_CASE'],
				modifiers: ['readonly', 'static'],
			},
			{
				selector: 'enumMember',
				format: ['UPPER_CASE'],
			},
			{
				selector: 'interface',
				format: ['PascalCase'],
				prefix: ['I'],
			},
			{
				selector: 'enum',
				format: ['PascalCase'],
				prefix: ['E'],
			},
			{
				selector: 'class',
				format: ['PascalCase'],
				modifiers: ['abstract'],
				prefix: ['Base'],
			},
			{
				selector: 'typeParameter',
				format: ['PascalCase'],
				prefix: ['T']
			},
		],
		'@typescript-eslint/no-base-to-string': 'error',
		'@typescript-eslint/no-confusing-non-null-assertion': 'off', // @typescript-eslint/no-non-null-assertion
		'@typescript-eslint/no-confusing-void-expression': 'off', // not care
		'@typescript-eslint/no-duplicate-enum-values': 'error',
		'@typescript-eslint/no-dynamic-delete': 'off', // not care
		'@typescript-eslint/no-extraneous-class': 'off', // not care
		// @typescript-eslint/no-implicit-any-catch // deprecated
		'@typescript-eslint/no-import-type-side-effects': 'off', // not care
		'@typescript-eslint/no-invalid-void-type': 'off', // not care
		'@typescript-eslint/no-meaningless-void-operator': 'error',
		'@typescript-eslint/no-mixed-enums': 'error',
		'@typescript-eslint/no-non-null-asserted-nullish-coalescing': 'off', // @typescript-eslint/no-non-null-assertion
		// @typescript-eslint/no-parameter-properties // deprecated
		'@typescript-eslint/no-redundant-type-constituents': 'error',
		'@typescript-eslint/no-require-imports': 'error',
		'@typescript-eslint/no-type-alias': 'off', // not care
		'@typescript-eslint/no-unnecessary-boolean-literal-compare': 'error',
		'@typescript-eslint/no-unnecessary-condition': 'error',
		'@typescript-eslint/no-unnecessary-qualifier': 'off', // @typescript-eslint/no-namespace
		'@typescript-eslint/no-unnecessary-type-arguments': 'error',
		'@typescript-eslint/no-unsafe-declaration-merging': 'error',
		'@typescript-eslint/no-useless-empty-export': 'error',
		'@typescript-eslint/non-nullable-type-assertion-style': 'off', // @typescript-eslint/no-non-null-assertion
		'@typescript-eslint/parameter-properties': 'off', // not care
		'@typescript-eslint/prefer-enum-initializers': 'error',
		'@typescript-eslint/prefer-for-of': 'off', // not care
		'@typescript-eslint/prefer-function-type': 'off', // not care
		'@typescript-eslint/prefer-includes': 'error',
		'@typescript-eslint/prefer-literal-enum-member': 'error',
		'@typescript-eslint/prefer-nullish-coalescing': 'error',
		'@typescript-eslint/prefer-optional-chain': 'error',
		'@typescript-eslint/prefer-readonly': 'error',
		'@typescript-eslint/prefer-readonly-parameter-types': 'off', // not care
		'@typescript-eslint/prefer-reduce-type-parameter': 'error',
		'@typescript-eslint/prefer-regexp-exec': 'error',
		'@typescript-eslint/prefer-return-this-type': 'error',
		'@typescript-eslint/prefer-string-starts-ends-with': 'error',
		'@typescript-eslint/prefer-ts-expect-error': 'off', // @typescript-eslint/ban-ts-comment
		'@typescript-eslint/promise-function-async': 'off', // not care
		'@typescript-eslint/require-array-sort-compare': 'error',
		'@typescript-eslint/sort-type-constituents': 'error',
		// @typescript-eslint/sort-type-union-intersection-members // deprecated
		'@typescript-eslint/strict-boolean-expressions': 'off', // not care
		'@typescript-eslint/switch-exhaustiveness-check': 'error',
		// @typescript-eslint/type-annotation-spacing // conflict with Prettier
		'@typescript-eslint/typedef': 'off', // not care
		'@typescript-eslint/unified-signatures': 'off', // not care

		//#endregion
		//#region extension rules

		// @typescript-eslint/block-spacing // use Prettier instead
		// @typescript-eslint/brace-style // conflict with Prettier
		// @typescript-eslint/comma-dangle // conflict with Prettier
		// @typescript-eslint/comma-spacing // conflict with Prettier
		'@typescript-eslint/default-param-last': 'error',
		'@typescript-eslint/dot-notation': 'error',
		// @typescript-eslint/func-call-spacing // conflict with Prettier
		// @typescript-eslint/indent // conflict with Prettier
		'@typescript-eslint/init-declarations': 'error',
		// '@typescript-eslint/key-spacing' // use Prettier instead
		// @typescript-eslint/keyword-spacing // conflict with Prettier
		'@typescript-eslint/lines-between-class-members': 'off', // not care
		'@typescript-eslint/no-dupe-class-members': 'off', // typescript compiler
		// @typescript-eslint/no-duplicate-imports // deprecated
		// @typescript-eslint/no-extra-parens // conflict with Prettier
		'@typescript-eslint/no-invalid-this': 'error',
		'@typescript-eslint/no-loop-func': 'error',
		'@typescript-eslint/no-magic-numbers': 'off', // not care
		'@typescript-eslint/no-redeclare': 'error',
		'@typescript-eslint/no-restricted-imports': [
			'error',
			{
				patterns: [
					{
						group: ['../**'],
						message:
							'Usage of relative parent imports is not allowed.',
					},
				],
			},
		],
		'@typescript-eslint/no-shadow': [
			'error',
			{ "ignoreOnInitialization": true }
		],
		'@typescript-eslint/no-throw-literal': [
			'error',
			{ allowThrowingAny: false, allowThrowingUnknown: false },
		],
		'@typescript-eslint/no-unused-expressions': 'error',
		'@typescript-eslint/no-use-before-define': 'off', // typescript compiler
		'@typescript-eslint/no-useless-constructor': 'error',
		// @typescript-eslint/object-curly-spacing // conflict with Prettier
		'@typescript-eslint/padding-line-between-statements': 'off', // not care
		// @typescript-eslint/quotes // conflict with Prettier
		'@typescript-eslint/return-await': 'error',
		// @typescript-eslint/semi // conflict with Prettier
		// @typescript-eslint/space-before-blocks // conflict with Prettier
		// @typescript-eslint/space-before-function-paren // conflict with Prettier
		// @typescript-eslint/space-infix-ops // conflict with Prettier

		//#endregion
	},
}
